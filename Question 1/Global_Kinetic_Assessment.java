import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.interactions.Actions;


import java.util.concurrent.TimeUnit;

public class Global_Kinetic_Assessment {

    public static  void main(String[] args) {
        // declaration and instantiation of objects/variables
        System.setProperty("webdriver.chrome.driver","C:\\Users\\gloka\\Downloads\\chromedriver_win32\\\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        //Launching the browser and directing it the url
        driver.get("http://automationpractice.com/index.php");

        //wait for 10 seconds for all elements to appear on the page
        driver.manage().timeouts().implicitlyWait(10 , TimeUnit.SECONDS);

        //Sing in Process
        driver.findElement(By.xpath("/html/body/div/div[1]/header/div[2]/div/div/nav/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[1]/form/div/div[2]/input")).sendKeys("glokas13@hotmail.test");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[1]/form/div/div[3]/button/span")).click();

        //Account creation and Registration process
        driver.findElement(By.cssSelector("input[type='radio'][value='1']")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[2]/input")).sendKeys("Glody");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[3]/input")).sendKeys("Kasongo");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[1]/div[5]/input")).sendKeys("12345");

        Select dropdown = new Select(driver.findElement(By.name("days")));
        dropdown.selectByValue("13");

        Select dropdown2 = new Select(driver.findElement(By.name("months")));
        dropdown2.selectByValue("4");

        Select dropdown3 = new Select(driver.findElement(By.name("years")));
        dropdown3.selectByValue("1988");

        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[3]/input")).sendKeys("Global Kinetic");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[4]/input")).sendKeys("8 Lebanon Close");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[5]/input")).sendKeys("4 Kalnor Gardens");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[6]/input")).sendKeys("Atlanta ");

        Select dropdown4 = new Select(driver.findElement(By.name("id_state")));
        dropdown4.selectByValue("10");

        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[8]/input")).sendKeys("30003");

        Select dropdown5 = new Select(driver.findElement(By.name("id_country")));
        dropdown5.selectByValue("21");

        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[13]/input")).sendKeys("+27 76 38 343 49");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[14]/input")).clear();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[2]/p[14]/input")).sendKeys("Glody's Address");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/div[4]/button/span")).click();

        //Sing out and Sign in again
        driver.findElement(By.xpath("/html/body/div/div[1]/header/div[2]/div/div/nav/div[2]/a")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[1]/input")).sendKeys("glokas13@hotmail.test");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/div[2]/span/input")).sendKeys("12345");
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[2]/form/div/p[2]/button/span")).click();

        //Mouse Hover Dresses Items
        Actions Select_dresses = new Actions(driver);
        WebElement Dresses = driver.findElement(By.xpath("/html/body/div/div[1]/header/div[3]/div/div/div[6]/ul/li[2]/a"));
        Select_dresses.moveToElement(Dresses).build().perform();

        Actions Select_Casual_dress = new Actions(driver);
        WebElement Casual_dress = driver.findElement(By.xpath("/html/body/div/div[1]/header/div[3]/div/div/div[6]/ul/li[2]/ul/li[1]/a"));
        Select_Casual_dress.moveToElement(Casual_dress).build().perform();
        Casual_dress.click();

        //Mouse Hover Casual dress
        Actions Hover_dress = new Actions(driver);
        WebElement Dress = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div[2]/ul/li"));
        Hover_dress.moveToElement(Dress).build().perform();

        //Add Dress to Cart and Proceed to check out
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div[2]/ul/li/div/div[2]/div[2]/a[1]/span")).click();
        driver.findElement(By.xpath("/html/body/div/div[1]/header/div[3]/div/div/div[4]/div[1]/div[2]/div[4]/a/span")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/p[2]/a[1]/span")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/p/button/span")).click();
        driver.findElement(By.id("cgv")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/form/p/button/span")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/div/div[3]/div[1]/div/p/a")).click();
        driver.findElement(By.xpath("/html/body/div/div[2]/div/div[3]/div/form/p/button/span")).click();

        //Sing out
        driver.findElement(By.xpath("/html/body/div/div[1]/header/div[2]/div/div/nav/div[2]/a")).click();
        //Closing the browser
        driver.close();
    }
}
